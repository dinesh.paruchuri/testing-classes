package Assignment2.Assignment2;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.*;
public class Cart {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\dparuchuri002\\eclipse-workspace\\Assignment2\\drivers\\chromedriver.exe");
		

		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.flipkart.com/");
		WebElement c=driver.findElement(By.xpath("//button[@class='_2KpZ6l _2doB4z']"));
		c.click();
		driver.manage().window().maximize();
		
		WebElement searchbar = driver.findElement(By.xpath("//input[@name='q']"));
		searchbar.sendKeys("iphone 14 pro max");
		
		driver.findElement(By.xpath("//button[@class='L0Z3Pu']")).submit();
		
//		Actions actions = new Actions(driver);
		WebDriverWait wt = new WebDriverWait(driver, 2);
		wt.until(ExpectedConditions.elementToBeClickable (By.xpath("//button[@class='L0Z3Pu']")));
		wt.until(ExpectedConditions.elementToBeClickable (By.xpath("//div[@data-id='MOBGHWFHXFGNUZJA']//a")));
		driver.findElement(By.xpath("//div[@data-id='MOBGHWFHXFGNUZJA']//a")).click();
		String originalWindow= driver.getWindowHandle();

		//Loop through until we find a new window handle
		for (String windowHandle : driver.getWindowHandles()) {
		    if(!originalWindow.contentEquals(windowHandle)) {
		        driver.switchTo().window(windowHandle);
		        break;
		    }
		}
		driver.findElement(By.xpath("//button[@class='_2KpZ6l _2U9uOA _3v1-ww']")).click();
		//driver.close();
	}

}
