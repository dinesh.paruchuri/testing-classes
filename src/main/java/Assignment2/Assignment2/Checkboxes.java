package Assignment2.Assignment2;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;
import java.util.List;
public class Checkboxes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\dparuchuri002\\eclipse-workspace\\Assignment2\\drivers\\chromedriver.exe");
		
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://the-internet.herokuapp.com/checkboxes");
		
		driver.manage().window().maximize();
		
		WebElement checkBox1 = driver.findElement(By.xpath("//*[@id=\"checkboxes\"]/input[1]"));
		boolean isDisplayed = checkBox1.isDisplayed();

		// performing click operation if element is displayed
		if (isDisplayed == true) {
			checkBox1.click();
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		}
		WebElement checkBox2 = driver.findElement(By.xpath("//*[@id=\"checkboxes\"]/input[2]"));
		boolean isDisplayed2 = checkBox2.isDisplayed();

		// performing click operation if element is displayed
		if (isDisplayed2 == true) {
			checkBox2.click();
			System.out.println("By Xpath done");
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		}
		
		
		WebElement checkBox11 = driver.findElement(By.cssSelector("input[type=checkbox]:nth-child(1)"));
		boolean isDisplayed3 = checkBox11.isDisplayed();
		// performing click operation if element is displayed
		if (isDisplayed3 == true) {
			checkBox11.click();
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		}
		WebElement checkBox22 = driver.findElement(By.cssSelector("input[type=checkbox]:nth-child(3)"));
		boolean isDisplayed4 = checkBox22.isDisplayed();

		// performing click operation if element is displayed
		if (isDisplayed4 == true) {
			checkBox22.click();
			System.out.println("By cssSelector done");
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		}
		
		//driver.close();
	}

}
